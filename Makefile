#SOLR_FOLDER=/opt/solr/solr-8.2.0
ZK=localhost:9983
SOLR_PORT=8983
SOLR_IP=localhost
SOLR_FOLDER=docker exec  --detach-keys 'ctrl-k'  -it $(DOCKER_HASH) /solr

deactivate_secu:
	http_proxy='' curl --user solr:$(SOLR_PASSWORD) http://$(SOLR_IP):$(SOLR_PORT)/solr/admin/authentication -H 'Content-type:application/json' -d  '{"set-property": {"blockUnknown":false}}'

activate_secu:
	http_proxy='' curl --user solr:$(SOLR_PASSWORD) http://$(SOLR_IP):$(SOLR_PORT)/solr/admin/authentication -H 'Content-type:application/json' -d  '{"set-property": {"blockUnknown":true}}'

solr-secu-init:
	#$(SOLR_FOLDER)/bin/solr zk cp file:solr/$(SOLR_INSTANCE)/security/security.json zk:/security.json -z $(ZK)
	#http_proxy='' curl --user solr:SolrRocks http://$(SOLR_IP):$(SOLR_PORT)/solr/admin/authentication -H 'Content-type:application/json' -d '{"set-user":{"solr":"$(SOLR_PASSWORD)" $(SOLR_PASSWORD_OTHER) }}'
	http_proxy='' curl --user solr:$(SOLR_PASSWORD) http://$(SOLR_IP):$(SOLR_PORT)/solr/admin/authentication -H 'Content-type:application/json' -d '{"set-user":{"solr":"$(SOLR_PASSWORD)" $(SOLR_PASSWORD_OTHER) }}'

solr-secu:
	http_proxy='' curl --user solr:$(SOLR_PASSWORD) http://$(SOLR_IP):$(SOLR_PORT)/solr/admin/authentication -H 'Content-type:application/json' -d '{"set-user":{"solr":"$(SOLR_PASSWORD)" $(SOLR_PASSWORD_OTHER) }}'

solr-create:
	$(SOLR_FOLDER)/server/scripts/cloud-scripts/zkcli.sh -cmd clear -z "$(ZK)"  /configs/$(collection)conf
	$(SOLR_FOLDER)/server/scripts/cloud-scripts/zkcli.sh -cmd upconfig  -confdir solr/$(SOLR_INSTANCE)/configsets/$(collection)/conf/  -confname $(collection)conf -z "$(ZK)"
	http_proxy='' curl --user $(SOLR_USER):$(SOLR_PASSWORD) "http://$(SOLR_IP):$(SOLR_PORT)/solr/admin/collections?action=CREATE&name=$(collection)&numShards=$(SOLR_SHARD)&replicationFactor=$(REPLICATION_FACTOR)&maxShardsPerNode=1&collection.configName=$(collection)conf"


solr-delete:
	http_proxy='' curl --user $(SOLR_USER):$(SOLR_PASSWORD) "http://$(SOLR_IP):$(SOLR_PORT)/solr/admin/collections?action=DELETE&name=$(collection)&deleteInstanceDir=true"

